<?php

namespace yii2portal\video\models;

use Yii;
use yii2portal\video\widgets\BaseWidget;

/**
 * This is the model class for table "video_els".
 *
 * @property integer $id
 * @property integer $pid
 * @property integer $ord
 * @property string $type
 * @property string $idkey
 */
class VideoEls extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video_els';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'ord'], 'integer'],
            [['type', 'idkey'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pid' => 'Pid',
            'ord' => 'Ord',
            'type' => 'Type',
            'idkey' => 'Idkey',
        ];
    }

    /**
     * @return  yii2portal\video\widgets\BaseWidget
     */
    public function getWidget(){
        $className = 'yii2portal\video\widgets\\' . ucfirst($this->type);
        $widget = Yii::createObject($className);
        if($widget && $widget instanceof BaseWidget){
            $widget->setKey($this->idkey);
        }

        return $widget;
    }

    /**
     * @inheritdoc
     * @return VideoElsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VideoElsQuery(get_called_class());
    }
}
