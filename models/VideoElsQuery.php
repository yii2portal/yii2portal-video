<?php

namespace yii2portal\video\models;

/**
 * This is the ActiveQuery class for [[VideoEls]].
 *
 * @see VideoEls
 */
class VideoElsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return VideoEls[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VideoEls|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
