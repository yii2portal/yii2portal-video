<?php

namespace yii2portal\video;

use Yii;
use yii2portal\video\components\cplugin\Video;

class Module extends \yii2portal\core\Module
{
    public function init()
    {
        parent::init();

        Yii::$app->getModule('cplugin')->registerPlugin(Video::className());
    }
}