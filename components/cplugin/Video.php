<?php

namespace yii2portal\video\components\cplugin;

use yii2portal\cplugin\models\Cplugin;
use yii2portal\video\models\VideoEls;


class Video extends Cplugin
{

    public function pluginChk($params)
    {
        return array(
            'status' => true,
            'params' => $params
        );
    }

    public function pluginConfig($params)
    {

        return array(
            'html' => $this->render('config', array('params' => $params)),
            'config' => true,
            'resizable' => false,
            'styles' => false
        );
    }

    public function pluginRender($params)
    {
        $return = '';

        $ids = explode(',', $params['ids']);

        $videos = VideoEls::find()
            ->where([
                'id' => $ids
            ])
            ->indexBy('id')
            ->all();

        if (!empty($videos)) {
            $sorted = [];

            foreach ($ids as $id) {
                if (isset($videos[$id])) {
                    $sorted[] = $videos[$id];
                }
            }

            $params['width'] = $this->getSize($params['width']);
            $return = $this->render('video', array('videos' => $sorted, 'params' => $params));
        }


        return $return;
    }
}