<?php

namespace yii2portal\video\widgets;

class Obs extends BaseWidget {

    public function getCode($width, $height,$params = array()) {
        $return = "";
        if(!empty($this->key)){
            $return = <<<EOF
<object width="{$width}" height="{$height}">
<param name="movie" value="//obs.kg/flowplayer/flowplayer.swf?config=http://obs.kg/embed/video/{$this->key}"></param>
<param name="allowFullScreen" value="true"></param>
<param name="allowScriptAccess" value="always"></param>
<embed src="//obs.kg/flowplayer/flowplayer.swf?config=//obs.kg/embed/video/{$this->key}" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="{$width}" height="{$height}"></embed></object>          
EOF;
        }
        return $return;
    }

   

    public function setKey($key) {
        $this->_key = $key;
        $this->_url = "http://www.obs.kg/view/{$key}/";
        return $this;
    }

    public function setUrl($url) {
        $this->_url = $url;
        $match = array();
        preg_match("~view/([^/]+)~i", $url, $match);
        $this->_key = $match[1];
        return $this;
    }

}

