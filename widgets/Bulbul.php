<?php

namespace yii2portal\video\widgets;

class Bulbul extends BaseWidget {

    public function getCode($width, $height,$params = array()) {
        $return = "";
        if(!empty($this->key)){
            $dir = $this->key%10;
            $return = <<<EOF
<object width="{$width}" height="{$height}">
    <param value="//www.bulbul.kg/player/flvplayer.swf" name="movie">
    <param value="true" name="allowfullscreen">
    <param value="always" name="allowscriptaccess">
    <param value="file=//static.bulbul.kg/flv/{$dir}/{$this->key}.flv&image=//static.bulbul.kg/img/{$dir}/{$this->key}.jpg&logo=//bulbul.kg/css/images/minilogo3.png" name="flashvars">
    <embed width="{$width}" height="{$height}" flashvars="file=//static.bulbul.kg/flv/{$dir}/{$this->key}.flv&image=//static.bulbul.kg/img/{$dir}/{$this->key}.jpg&logo=//bulbul.kg/css/images/minilogo3.png" allowfullscreen="true" allowscriptaccess="always" src="//www.bulbul.kg/player/flvplayer.swf" type="application/x-shockwave-flash">
</object>
EOF;
        }
        return $return;
    }

   

    public function setKey($key) {
        $this->_key = $key;
        $this->_url = "http://www.bulbul.kg/video:{$key}/";
        return $this;
    }

    public function setUrl($url) {
        $this->_url = $url;
        $match = array();
        preg_match("~video:(\d+)~i", $url, $match);
        $this->_key = $match[1];
        return $this;
    }

}

