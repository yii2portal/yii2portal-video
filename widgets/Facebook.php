<?php

namespace yii2portal\video\widgets;

class Facebook extends BaseWidget {

    public function getCode($width, $height,$params = array()) {
        $return = "";
        if(!empty($this->key)){
            $return = <<<EOF
<iframe width="{$width}" height="{$height}" src="//www.facebook.com/video/embed?video_id={$this->key}" frameborder="0" allowfullscreen></iframe>
EOF;
        }
        return $return;
    }

   

    public function setKey($key) {
        $this->_key = $key;
        $this->_url = "https://www.facebook.com/video/embed?video_id={$key}/";
        return $this;
    }

    public function setUrl($url) {
        $this->_url = $url;
        
        $url = parse_url($this->_url);
        $match = array();
        preg_match("~v=([\d]+)~i", $url['query'], $match);

        $this->_key = $match[1];
        return $this;
    }

}

