<?php

namespace yii2portal\video\widgets;

class Blive extends BaseWidget {

    public function getCode($width, $height,$params = array()) {
        $return = "";
        if(!empty($this->key)){
            $return = <<<EOF
         <object width='{$width}' height='{$height}' data='//static.blive.kg/player/player.swf' type='application/x-shockwave-flash'>
    <param name='flashvars' value='config=//www.blive.kg/getvideo:{$this->key}/' />
    <param name='movie' value='//static.blive.kg/player/player.swf' />
    <param name='allowfullscreen' value='true' />
</object>           
EOF;
        }
        return $return;
    }

    public function setKey($key) {
        $this->_key = $key;
        $this->_url = "http://www.blive.kg/video:{$key}/";
        return $this;
    }

    public function setUrl($url) {
        $this->_url = $url;
        $match = array();
        preg_match("~video:(\d+)~", $url, $match);
        $this->_key = $match[1];
        return $this;
    }

}
