<?php

namespace yii2portal\video\widgets;

class Namba extends BaseWidget {

    public function getCode($width, $height,$params = array()) {
        $return = "";
        if(!empty($this->key)){
            $return = <<<EOF
<object height="{$height}" width="{$width}" type="application/x-shockwave-flash" data="//video.namba.kg/swf/player/3.2.11/flowplayer-3.2.11.swf">
    <param value="true" name="allowfullscreen">
    <param value="always" name="allowscriptaccess">
    <param name="src" value="//video.namba.kg/swf/player/3.2.11/flowplayer-3.2.11.swf" />
    <param value="config=//video.namba.kg/flashvars-3.2.11.php?i={$this->key}_0" name="flashvars">
    </object>
EOF;
        }
        return $return;
    }

   

    public function setKey($key) {
        $this->_key = $key;
        list(,,$key) = explode("_",$key);
        $this->_url = "http://namba.kg/video_player.php?id={$key}&nambaPlayer=0/";
        return $this;
    }

    public function setUrl($url) {
        $this->_url = $url;
        $match = array();
        preg_match("~video/(\d+)~i", $url, $match);
        $id = $match[1];
        $content = $this->__readPage("http://namba.kg/video_player.php?id={$id}&nambaPlayer=0");
        $match = array();
        
        preg_match("~\.php\?i=([\d_]+)~i", $content, $match);
        $this->_key = $match[1];
        return $this;
    }
    
    private function __readPage($url) {
        if (!function_exists('curl_init')) {
            return false;
        }

        //sleep(5 + rand(1, 3)); // ANTI_ANTI_BOT :)

        $BROWSER = "Mozilla/5.0 (Windows; U; Windows NT 5.1; ru-RU; rv:1.9.0.1) Gecko/2008070208 YFF3 Firefox/4.0";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, $BROWSER);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        

        return curl_exec($ch);
    }

}

/**

 http://namba.kg/video_player.php?id=4363851&nambaPlayer=0
<object height="385" width="640" type="application/x-shockwave-flash" data="http://video.namba.kg/swf/player/3.2.11/flowplayer-3.2.11.swf">
    <param value="true" name="allowfullscreen">
    <param value="opaque" name="wmode">
    <param value="always" name="allowscriptaccess">
    <param name="src" value="http://video.namba.kg/swf/player/3.2.11/flowplayer-3.2.11.swf" />
    <param value="config=http://video.namba.kg/flashvars-3.2.11.php?i=46315291_46315261__4363851_1" name="flashvars">
</object>

 */
