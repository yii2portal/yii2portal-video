<?php


namespace yii2portal\video\widgets;

use yii2portal\core\components\Widget as YiiWidget;

abstract class BaseWidget extends YiiWidget
{
    protected $_url;
    protected $_key;
    protected $_code;

    abstract public function setUrl($url);

    abstract public function setKey($key);

    public function getKey()
    {
        return $this->_key;
    }

    public function getUrl()
    {
        return $this->_url;
    }

    public function getType()
    {
        return strtolower(substr(get_class($this), 8));
    }

    public function isValid()
    {
        return !empty($this->_key) && !empty($this->_url);
    }

    abstract public function getCode($width, $height, $params = array());
}