<?php
namespace yii2portal\video\widgets;

class Smotri extends BaseWidget {

    public function getCode($width, $height,$params = array()) {
        $return = "";
        if(!empty($this->key)){
            $return = <<<EOF
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="{$width}" height="{$height}">
	<param name="movie" value="//pics.smotri.com/player.swf?file={$this->key}&bufferTime=3&autoStart=false&str_lang=rus&xmlsource=%2F%2Fpics.smotri.com%2Fcskins%2Fblue%2Fskin_color.xml&xmldatasource=%2F%2Fpics.smotri.com%2Fskin_ng.xml" />
	<param name="allowScriptAccess" value="always" />
	<param name="allowFullScreen" value="true" />
	<param name="bgcolor" value="#ffffff" />
	<embed src="//pics.smotri.com/player.swf?file={$this->key}&bufferTime=3&autoStart=false&str_lang=rus&xmlsource=%2F%2Fpics.smotri.com%2Fcskins%2Fblue%2Fskin_color.xml&xmldatasource=%2F%2Fpics.smotri.com%2Fskin_ng.xml" quality="high" allowscriptaccess="always" allowfullscreen="true" wmode="opaque"  width="{$width}" height="{$height}" type="application/x-shockwave-flash"></embed>
</object>
EOF;
        }
        return $return;
    }

   

    public function setKey($key) {
        $this->_key = $key;
        $this->_url = "http://smotri.com/video/view/?id={$key}";
        return $this;
    }

    public function setUrl($url) {
        $this->_url = $url;
        $match = array();
        preg_match("~id=([^/#]+)~i", $url, $match);
        $this->_key = $match[1];

        return $this;
    }

}

