<?php

namespace yii2portal\video\widgets;

class Vimeo extends BaseWidget {

    public function getCode($width, $height,$params = array()) {
        $return = "";
        if(!empty($this->key)){
            $return = <<<EOF
         <iframe src="//player.vimeo.com/video/{$this->key}?byline=0&amp;portrait=0" width="{$width}" height="{$height}" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
EOF;
        }
        return $return;
    }

    public function setKey($key) {
        $this->_key = $key;
        $this->_url = "http://vimeo.com/{$key}/";
        return $this;
    }

    public function setUrl($url) {
        $this->_url = $url;
        $match = array();
        preg_match("~(\d+)~", $url, $match);
        $this->_key = $match[1];
        return $this;
    }

}
