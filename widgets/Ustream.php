<?php

namespace yii2portal\video\widgets;

class Ustream extends BaseWidget {

    public function getCode($width, $height,$params = array()) {
        $return = "";
        if(!empty($this->key)){
            $return = <<<EOF
<iframe width="{$width}" height="{$height}" src="//www.ustream.tv/embed/{$this->key}?v=3&amp;wmode=direct" frameborder="0" allowfullscreen></iframe>
EOF;
        }
        return $return;
    }

   

    public function setKey($key) {
        $this->_key = $key;
        $this->_url = "http://www.ustream.tv/embed/{$key}/";
        return $this;
    }

    public function setUrl($url) {
        $this->_url = $url;
        
        $url = parse_url($this->_url);
        $match = array();
        preg_match("~channel/([\d]+)~i", $url['path'], $match);

        $this->_key = $match[1];
        return $this;
    }

}

