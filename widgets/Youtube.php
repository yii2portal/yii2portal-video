<?php

namespace yii2portal\video\widgets;

class Youtube extends BaseWidget {

    public function getCode($width, $height, $params = array()) {
        $return = "";
        if (!empty($this->key)) {
            $return = <<<EOF
        <iframe width="{$width}" height="{$height}" src="//www.youtube.com/embed/{$this->key}" frameborder="0" allowfullscreen></iframe>
EOF;
        }
        return $return;
    }

    public function setKey($key) {
        $this->_key = $key;
        $this->_url = "http://www.youtube.com/watch?v={$key}/";
        return $this;
    }

    public function setUrl($url) {
        $this->_url = $url;
        $match = array();
        preg_match("~v=([^&/]+)~i", $url, $match);
        $this->_key = $match[1];
        return $this;
    }

}
